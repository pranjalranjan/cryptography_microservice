cd /app/lib
unzip java_12.zip
cd /app/vault
unzip vault_1.2.0_linux_amd64.zip
./vault server -dev >> output.txt &
sleep 10s
grep "Root Token: " output.txt >> rootTokenFile.txt
ROOT_TOKEN_VALUE=$(cut -d ':' -f 2 rootTokenFile.txt | tr -d ' ')
rm output.txt
rm rootTokenFile.txt
export VAULT_ADDR='http://127.0.0.1:8200'
./vault secrets enable transit
./vault write -f transit/keys/cryptographic-service-key
cd /app/svc
jdk-12.0.2/bin/java -jar -Dservice.token="$ROOT_TOKEN_VALUE" cryptographicservice-1.0-SNAPSHOT.jar