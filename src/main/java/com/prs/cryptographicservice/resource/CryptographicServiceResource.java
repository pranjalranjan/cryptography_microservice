package com.prs.cryptographicservice.resource;

import com.prs.cryptographicservice.runnable.DecryptionRunnable;
import com.prs.cryptographicservice.runnable.EncryptionRunnable;
import com.prs.cryptographicservice.threadpool.DecryptionThreadPool;
import com.prs.cryptographicservice.threadpool.EncryptionThreadPool;
import com.prs.cryptographicservice.vo.DecryptionInputPayload;
import com.prs.cryptographicservice.vo.EncryptionInputPayload;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.container.TimeoutHandler;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.TimeUnit;

@Path("/v1/cryptographicservice")
public class CryptographicServiceResource
{
    private EncryptionThreadPool encryptionThreadPool;
    private DecryptionThreadPool decryptionThreadPool;

    public CryptographicServiceResource()
    {
        this.encryptionThreadPool = EncryptionThreadPool.getInstance();
        this.decryptionThreadPool = DecryptionThreadPool.getInstance();
    }

    @Path("/encryption")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void encryptText(EncryptionInputPayload inputPayload , final @Suspended AsyncResponse response)
    {
        response.setTimeout(60, TimeUnit.SECONDS);
        response.setTimeoutHandler(
                new TimeoutHandler() {
                    @Override
                    public void handleTimeout(AsyncResponse asyncResponse) {
                        response.resume(Response.serverError().build());
                    }
                }
        );
        encryptionThreadPool.submitTask(new EncryptionRunnable(inputPayload, response));
    }

    @Path("/decryption")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void decryptText(DecryptionInputPayload inputPayload, final @Suspended AsyncResponse response)
    {
        response.setTimeout(60, TimeUnit.SECONDS);
        response.setTimeoutHandler(
                new TimeoutHandler() {
                    @Override
                    public void handleTimeout(AsyncResponse asyncResponse) {
                        response.resume(Response.serverError().build());
                    }
                }
        );
        decryptionThreadPool.submitTask(new DecryptionRunnable(inputPayload, response));
    }
}
