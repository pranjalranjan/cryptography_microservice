package com.prs.cryptographicservice.threadpool;

import com.prs.cryptographicservice.runnable.DecryptionRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DecryptionThreadPool
{
    private static volatile DecryptionThreadPool decryptionThreadPool;
    private ExecutorService executorService;

    private DecryptionThreadPool()
    {
        executorService = Executors.newFixedThreadPool(20);
    }

        public static DecryptionThreadPool getInstance()
    {
        if(decryptionThreadPool == null)
        {
            synchronized (EncryptionThreadPool.class)
            {
                if(decryptionThreadPool == null)
                {
                    decryptionThreadPool = new DecryptionThreadPool();
                }
            }
        }
        return decryptionThreadPool;
    }

    public void submitTask(DecryptionRunnable runnable)
    {
        executorService.submit(runnable);
    }

    public void shutdown()
    {
        executorService.shutdown();
    }

    public boolean isTerminated()
    {
        return executorService.isTerminated();
    }

    public boolean isShutDown()
    {
        return executorService.isShutdown();
    }
}
