package com.prs.cryptographicservice.threadpool;

import com.prs.cryptographicservice.runnable.EncryptionRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EncryptionThreadPool
{
    private static volatile EncryptionThreadPool encryptionThreadPool;
    private ExecutorService executorService;

    private EncryptionThreadPool()
    {
        executorService = Executors.newFixedThreadPool(10);
    }

    public static EncryptionThreadPool getInstance()
    {
        if(encryptionThreadPool == null)
        {
            synchronized (EncryptionThreadPool.class)
            {
                if(encryptionThreadPool == null)
                {
                    encryptionThreadPool = new EncryptionThreadPool();
                }
            }
        }
        return encryptionThreadPool;
    }

    public void submitTask(EncryptionRunnable runnable)
    {
        executorService.submit(runnable);
    }

    public void shutdown()
    {
        executorService.shutdown();
    }

    public boolean isTerminated()
    {
        return executorService.isTerminated();
    }

    public boolean isShutDown()
    {
        return executorService.isShutdown();
    }
}
