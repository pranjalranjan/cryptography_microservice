package com.prs.cryptographicservice.constants;

public class CryptographicServiceConstants
{
    public static String VAULT_TRANSIT_SECRETS_ENGINE_URL="http://127.0.0.1:8200";
    public static String ENCRYPTION_ENDPOINT = "/v1/transit/encrypt";
    public static String DECRYPTION_ENDPOINT = "/v1/transit/decrypt";
    public static String KEY_NAME = "cryptographic-service-key";
    public static String TOKEN = "";
    public static String SECRET_TOKEN_PROPERTY_NAME = "service.token";

    public static void setTokenValue(String token)
    {
        CryptographicServiceConstants.TOKEN = token;
    }
}
