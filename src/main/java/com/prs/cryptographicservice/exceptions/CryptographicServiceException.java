package com.prs.cryptographicservice.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class CryptographicServiceException extends Throwable implements ExceptionMapper<Throwable>
{

    private static final long serialVersionUID = 1L;
    private String message;

    public CryptographicServiceException(String message)
    {
        this.message = message;
    }

    public CryptographicServiceException()
    {
        this.message = "Internal Server Error. Please try again!";
    }

    @Override
    public Response toResponse(Throwable throwable)
    {
        return Response.status(500).entity(message).type("text/plain").build();
    }
}
