package com.prs.cryptographicservice.helper;

import java.util.Base64;

public class Base64Helper
{
    public String base64Encode(String clearText)
    {
        return Base64.getEncoder().encodeToString(clearText.getBytes());
    }

    public String base64Decode(String base64EncodedString)
    {
        return new String(Base64.getDecoder().decode(base64EncodedString));
    }
}
