package com.prs.cryptographicservice.helper;

import com.prs.cryptographicservice.constants.CryptographicServiceConstants;
import com.prs.cryptographicservice.exceptions.CryptographicServiceException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class RESTConnectionHelper
{
    public String doPost(String urlString , String payload)
    {
        HttpURLConnection connection = null;
        StringBuilder response = new StringBuilder();
        try
        {
            URL url = new URL(urlString);

            connection = this.getConnection(url);
            connection.setRequestMethod("POST");

            OutputStream outputStream = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
            writer.write(payload);
            writer.flush();
            writer.close();
            outputStream.close();

            connection.connect();
            int responseCode = connection.getResponseCode();

            InputStream inputStream;
            if(isErrorCode(responseCode))
            {
                inputStream = connection.getErrorStream();
            }
            else
            {
                inputStream = connection.getInputStream();
            }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String responseLine = null;

            while((responseLine = bufferedReader.readLine()) != null)
            {
                response.append(responseLine.trim());
            }
        }
        catch (ProtocolException e)
        {
            return (new CryptographicServiceException("Protocol Unsupported!")).toResponse(e).toString();
        }
        catch (MalformedURLException e)
        {
            return (new CryptographicServiceException()).toResponse(e).toString();
        }
        catch (IOException e)
        {
            return (new CryptographicServiceException()).toResponse(e).toString();
        }finally
        {
            if(connection != null)
            {
                connection.disconnect();
            }
        }
        return response.toString();
    }

    private HttpURLConnection getConnection(URL url)
    {
        HttpURLConnection connection = null;
        try
        {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Vault-Token" , CryptographicServiceConstants.TOKEN);
            connection.setRequestProperty("Accept" , "*/*");
            connection.setDoOutput(true);
            connection.setDoInput(true);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.out.println(e.getCause().toString());

            if(connection != null)
            {
                connection.disconnect();
            }
        }

        return connection;
    }

    private boolean isErrorCode(int responseCode)
    {
        return responseCode < 200 || responseCode >=300;
    }

}
