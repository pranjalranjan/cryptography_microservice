package com.prs.cryptographicservice;

import com.prs.cryptographicservice.constants.CryptographicServiceConstants;
import com.prs.cryptographicservice.resource.CryptographicServiceResource;
import com.prs.cryptographicservice.threadpool.DecryptionThreadPool;
import com.prs.cryptographicservice.threadpool.EncryptionThreadPool;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LaunchService
{
    public static final URI BASE_URI = getBaseURI();

    private static EncryptionThreadPool encryptionThreadPool;
    private static DecryptionThreadPool decryptionThreadPool;

    private static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void main(String[] args) throws IOException
    {
        HttpServer server = null;
        try
        {
            try
            {
                server = startServer();
            } catch (Throwable t)
            {
                if(logger.isLoggable(Level.SEVERE))
                {
                    logger.log(Level.SEVERE, "Error starting up the server : " + t.getMessage() , t);
                }
                throw t;
            }
        }
        finally
        {
            stopThreadPools();

            if(server!=null)
            {
                server.shutdownNow();
            }

            System.exit(0);
        }
    }

    private static void stopThreadPools()
    {
        if(encryptionThreadPool != null)
        {
            encryptionThreadPool.shutdown();

            while(!encryptionThreadPool.isTerminated())
            {
                //Wait.
            }
        }

        if(decryptionThreadPool != null)
        {
            decryptionThreadPool.shutdown();

            while(!decryptionThreadPool.isTerminated())
            {
                //Wait.
            }
        }
    }

    private static void startThreadPools()
    {
        encryptionThreadPool = EncryptionThreadPool.getInstance();
        decryptionThreadPool = DecryptionThreadPool.getInstance();
    }

    protected static HttpServer startServer() throws IOException
    {
        ResourceConfig config = new ResourceConfig();
        config.register(CryptographicServiceResource.class);

        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, config, false);

        if(logger.isLoggable(Level.INFO))
        {
            logger.log(Level.INFO, "Jersey starting up.");
        }

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run()
            {
                if(logger.isLoggable(Level.INFO))
                {
                    logger.log(Level.INFO, "Stopping server..");
                }
                server.stop();
            }
        }, "shutdownHook"));

        try
        {
            server.start();

            if(logger.isLoggable(Level.INFO))
            {
                logger.log(Level.INFO, "Jersey started.");
            }

            setTokenValue();
            startThreadPools();

            if(logger.isLoggable(Level.INFO))
            {
                logger.log(Level.INFO, "Ready to serve requests!");
            }

            Thread.currentThread().join();
        } catch (Exception e)
        {
            if(logger.isLoggable(Level.SEVERE))
            {
                logger.log(Level.SEVERE, "There was an error while starting Grizzly HTTP server. The error : /n" + e.getMessage(), e);
            }
        }

        return server;
    }

    private static URI getBaseURI()
    {
        return UriBuilder.fromUri("http://0.0.0.0/").port(getPort(9998)).build();
    }

    private static int getPort(int defaultPort)
    {
        String port = System.getenv("HTTP_PORT");

        if(port != null)
        {
            try
            {
                return Integer.parseInt(port);
            }
            catch(NumberFormatException e)
            {

            }
        }
        return defaultPort;
    }


    private static void setTokenValue()
    {
        String secretToken = (System.getProperty(CryptographicServiceConstants.SECRET_TOKEN_PROPERTY_NAME)).trim();
        CryptographicServiceConstants.setTokenValue(secretToken);
    }
}
