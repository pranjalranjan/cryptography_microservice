package com.prs.cryptographicservice.vo;

public class EncryptionInputPayload
{
    public String plaintext;

    public EncryptionInputPayload(String plaintext)
    {
        this.plaintext = plaintext;
    }

    public EncryptionInputPayload()
    {

    }

    public String getPlaintext()
    {
        return plaintext;
    }

    public void setPlaintext(String plaintext)
    {
        this.plaintext = plaintext;
    }
}
