package com.prs.cryptographicservice.vo;

public class DecryptionInputPayload
{
    public String ciphertext;

    public DecryptionInputPayload(String ciphertext)
    {
        this.ciphertext = ciphertext;
    }

    public DecryptionInputPayload()
    {

    }

    public void setCiphertext(String ciphertext)
    {
        this.ciphertext = ciphertext;
    }

    public String getCiphertext()
    {
        return ciphertext;
    }
}
