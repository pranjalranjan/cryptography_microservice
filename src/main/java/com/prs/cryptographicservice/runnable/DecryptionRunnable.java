package com.prs.cryptographicservice.runnable;

import com.prs.cryptographicservice.constants.CryptographicServiceConstants;
import com.prs.cryptographicservice.helper.Base64Helper;
import com.prs.cryptographicservice.helper.RESTConnectionHelper;
import com.prs.cryptographicservice.vo.DecryptionInputPayload;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class DecryptionRunnable implements Runnable
{

    private String ciphertext;
    private AsyncResponse response;
    Base64Helper base64Helper;
    RESTConnectionHelper restConnectionHelper;

    public DecryptionRunnable(DecryptionInputPayload decryptionInputPayload, final @Suspended AsyncResponse response)
    {
        this.ciphertext = decryptionInputPayload.getCiphertext();
        this.response = response;
        base64Helper = new Base64Helper();
        restConnectionHelper = new RESTConnectionHelper();
    }

    public void run()
    {
        String payload = "{\"ciphertext\" : \"" + ciphertext + "\" }";

        String urlString = CryptographicServiceConstants.VAULT_TRANSIT_SECRETS_ENGINE_URL + CryptographicServiceConstants.DECRYPTION_ENDPOINT + "/" + CryptographicServiceConstants.KEY_NAME;

        String responseString = restConnectionHelper.doPost(urlString, payload);

        JSONObject object = new JSONObject(responseString);

        Response returnableResponse = null;

        int status = 500;
        String message = "";

        try
        {
            JSONObject data = object.getJSONObject("data");
            String plainTextB64Encoded = data.getString("plaintext");
            String plainText = base64Helper.base64Decode(plainTextB64Encoded);
            responseString = "{ \"plaintext\": \"" + plainText + "\"}";
            status = 200;
            message = responseString;
        }
        catch (JSONException je)
        {
            status = 500;
            message = responseString;
        }
        catch (Exception e)
        {
            status = 500;
            message = "An internal error occured";
        }
        finally
        {
            returnableResponse = Response.status(status).entity(message).type(MediaType.APPLICATION_JSON).build();
            this.response.resume(returnableResponse);
        }
    }
}
