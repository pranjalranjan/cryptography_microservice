package com.prs.cryptographicservice.runnable;

import com.prs.cryptographicservice.constants.CryptographicServiceConstants;
import com.prs.cryptographicservice.helper.Base64Helper;
import com.prs.cryptographicservice.helper.RESTConnectionHelper;
import com.prs.cryptographicservice.vo.EncryptionInputPayload;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class EncryptionRunnable implements Runnable
{
    private String plainText = "";
    private AsyncResponse response;
    private Base64Helper base64Helper;
    RESTConnectionHelper restConnectionHelper;

    public EncryptionRunnable(EncryptionInputPayload payload, final @Suspended AsyncResponse response)
    {
        this.plainText = payload.getPlaintext();
        this.response = response;
        base64Helper = new Base64Helper();
        restConnectionHelper = new RESTConnectionHelper();
    }

    public void run()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\"");
        String base64EncodedString = base64Helper.base64Encode(this.plainText);
        stringBuilder.append(base64EncodedString);
        stringBuilder.append("\"");

        String payload = "{\"plaintext\" : " + stringBuilder.toString() + "}";

        String urlString = CryptographicServiceConstants.VAULT_TRANSIT_SECRETS_ENGINE_URL + CryptographicServiceConstants.ENCRYPTION_ENDPOINT + "/" + CryptographicServiceConstants.KEY_NAME;

        String responseString = restConnectionHelper.doPost(urlString, payload);

        JSONObject object = new JSONObject(responseString);

        Response returnableResponse = null;

        int status = 500;
        try
        {
            JSONObject data = object.getJSONObject("data");
            responseString = (object.getJSONObject("data")).toString();
            status = 200;
        } catch (JSONException e)
        {
            //Status already handled and response already contains error.
        }
        finally
        {
            returnableResponse = Response.status(status).entity(responseString).type(MediaType.APPLICATION_JSON).build();
            this.response.resume(returnableResponse);
        }
    }
}
