FROM ubuntu:16.04
MAINTAINER pranjalranjan2010@gmail.com
RUN apt-get update && apt-get install unzip
COPY vault_1.2.0_linux_amd64.zip /app/vault/vault_1.2.0_linux_amd64.zip
COPY java_12.zip /app/lib/java_12.zip
COPY cryptographicservice-1.0-SNAPSHOT.jar /app/svc/cryptographicservice-1.0-SNAPSHOT.jar
COPY runService.sh /app/runService.sh
ENV JAVA_HOME="/app/lib/jdk-12.0.2"
EXPOSE 9998
ENTRYPOINT /app/runService.sh ; /bin/bash